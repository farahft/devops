package com.example.demo;

import java.util.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api2")

public class AddBank  {
    @Autowired
    BankService bankService;

    @PostMapping("")
    public void add(@RequestBody Bank bank) {
        bankService.saveBank(bank);
    }
    

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Bank bank , @PathVariable Integer id) {
        try {
        	
            Bank existBank = bankService.getBank(id);
            // i replaced bank by existBank
            existBank.setId(id);      
            bankService.saveBank(existBank);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
   
    
}
