package com.example.demo;

import java.util.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api1")
public class GestBank {

    @Autowired
    BankService bankService;
    
    @GetMapping("")
    public List<Bank> list() {
        return bankService.listAllUser();
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Bank> get(@PathVariable Integer id) {
        try {
            Bank bank = bankService.getBank(id);
            return new ResponseEntity<Bank>(bank, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Bank>(HttpStatus.NOT_FOUND);
        }
    }
    
}
