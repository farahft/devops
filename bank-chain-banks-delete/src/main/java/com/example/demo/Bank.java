package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="bank")
public class Bank {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String nom ;
    private int solde;
    
    public Bank(int id, String name, int solde) {
        this.id = id;
        this.nom = name;
        this.solde=solde;
    }
    
    public Bank() {
        this.id = 5;
        this.nom ="bh";
        this.solde=100;
    }
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return nom;
	}
	public void setName(String name) {
		this.nom = name;
	}
	public int getSolde() {
		return solde;
	}
	public void setSolde(int solde) {
		this.solde= solde;
	}
    
}
