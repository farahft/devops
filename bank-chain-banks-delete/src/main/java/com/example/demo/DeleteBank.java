package com.example.demo;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api3")

public class DeleteBank  implements Consume  {
	@Autowired
	   Consume c;


    @Autowired
    BankService bankService;
    
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {

        bankService.deleteBank(id);
    }
    
    @RequestMapping("/consume")
    public List<Bank> list() {
    	return c.list();
    	
    }
    
}
