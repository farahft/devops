package com.example.demo;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name="BANK-CHAIN-BANKS")
public interface Consume {
	@RequestMapping(value = "/consume")
    public List<Bank> list();
}
