package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.transaction.Transactional;
@Service
@Transactional
public class BankService {
    @Autowired
    private BankRepo bankRepo;
    
    public List<Bank> listAllUser() {
        return bankRepo.findAll();
    }

    public void saveBank(Bank bank) {
    	bankRepo.save(bank);
    }

    public Bank getBank(Integer id) {
        return bankRepo.findById(id).get();
    }

    public void deleteBank(Integer id) {
    	bankRepo.deleteById(id);
    }
}